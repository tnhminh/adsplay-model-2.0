package adsplay.delivery.redis.cache.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class RedisModel {

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
