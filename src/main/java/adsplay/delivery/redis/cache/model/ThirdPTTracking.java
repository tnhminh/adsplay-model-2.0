package adsplay.delivery.redis.cache.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThirdPTTracking extends RedisModel{

    private int id;

    @Expose
    @SerializedName("event")
    private String event;

    @Expose
    @SerializedName("url")
    private String url;

    public ThirdPTTracking(int id, String event, String url) {
        super();
        this.id = id;
        this.event = event;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
