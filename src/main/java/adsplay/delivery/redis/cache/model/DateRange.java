package adsplay.delivery.redis.cache.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DateRange extends RedisModel {

    @Expose
    @SerializedName("from")
    private String from;
    
    @Expose
    @SerializedName("to")
    private String to;

    public DateRange(String from, String to) {
        super();
        this.from = from;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}
