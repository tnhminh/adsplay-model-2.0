package adsplay.delivery.redis.cache.model;

import com.google.gson.annotations.Expose;

public class StartTime  extends RedisModel{

    @Expose
    private String type;

    @Expose
    private String value;

    public StartTime(String type, String value) {
        super();
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
