package adsplay.delivery.redis.cache.model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Creative extends RedisModel {

    public static String KEY_NAME = "creative";

    @SerializedName("crtId")
    private int creativeId;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("source")
    private String source;

    @Expose
    @SerializedName("landing_page")
    private String landingPage;

    @Expose
    @SerializedName("skip_time")
    private String skipTime = "";

    @Expose
    @SerializedName("duration")
    private String duration = "";

    @Expose
    @SerializedName("type")
    private String type = "local";

    @Expose
    @SerializedName("start_time")
    private StartTime startTime;

    @Expose
    @SerializedName("third_trackings")
    private List<ThirdPTTracking> list3rdTracking = new ArrayList<ThirdPTTracking>();

    public Creative() {
    }

    public Creative(int creativeId, String source, String landingPage, String skipTime, String duration, String type, StartTime startTime) {
        super();
        this.creativeId = creativeId;
        this.source = source;
        this.landingPage = landingPage;
        this.skipTime = skipTime;
        this.duration = duration;
        this.type = type;
        this.startTime = startTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ThirdPTTracking> getList3rdTracking() {
        return list3rdTracking;
    }

    public void setList3rdTracking(List<ThirdPTTracking> list3rdTracking) {
        this.list3rdTracking = list3rdTracking;
    }

    public int getCreativeId() {
        return creativeId;
    }

    public void setCreativeId(int creativeId) {
        this.creativeId = creativeId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

    public String getSkipTime() {
        return skipTime;
    }

    public void setSkipTime(String skipTime) {
        this.skipTime = skipTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public StartTime getStartTime() {
        return startTime;
    }

    public void setStartTime(StartTime startTime) {
        this.startTime = startTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static void main(String[] args) throws FileNotFoundException {

    }
}
