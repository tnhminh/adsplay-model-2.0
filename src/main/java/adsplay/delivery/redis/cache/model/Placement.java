package adsplay.delivery.redis.cache.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Placement extends RedisModel {

    public static final String MAC_OS = "Mac OS";
    public static final String I_PHONE = "iPhone";

    public static final String KEY_NAME = "placement";

    // Desktop/Mobile Web
    public static final int FPT_PLAY_WEB_LIVE_TV = 101;
    public static final int FPT_PLAY_WEB_VOD = 102;
    public static final int FPT_PLAY_WEB_MASTHEAD = 113;
    public static final int FSHARE_WEB_MASTHEAD = 115;
    public static final int FSHARE_TVC_PLACEMENT = 120;

    // Smart-TV
    public static final int FPT_PLAY_SMARTTV_LIVE_TV = 201;
    public static final int FPT_PLAY_SMART_TV_VOD = 202;
    public static final int FPT_PLAY_SMART_TV_MASTHEAD = 213;

    // Mobile/Native app
    public static final int FPT_PLAY_LIVE_STREAM_IOS = 301;
    public static final int FPT_PLAY_VOD_IOS = 302;
    public static final int FPT_PLAY_LIVE_STREAM_ANDROID_BOX = 303;
    public static final int FPT_PLAY_VOD_ANDROID_BOX = 304;
    public static final int FPT_PLAY_LIVE_STREAM_ANDROID_APP = 305;
    public static final int FPT_PLAY_VOD_ANDROID_APP = 306;
    public static final int FPT_PLAY_LIVE_STREAM_ANDROID_SMART_TV = 307;
    public static final int FPT_PLAY_VOD_ANDROID_SMART_TV = 308;

    // Mobile Masthead
    public static final int FPT_PLAY_ANDROID_MASTHEAD_EPL = 313;
    public static final int FPT_PLAY_IOS_MASTHEAD = 314;
    public static final int FPT_PLAY_ANDROID_MASTHEAD_U20 = 315;
    public static final int FPT_PLAY_ANDROID_INFEED_HOME = 317;
    public static final int FPT_PLAY_IOS_INFEED_HOME = 318;

    public static final int FPT_PLAY_MOBILE_DEV_TEST = 333;

    @Expose
    @SerializedName("ad_view")
    private String adView;

    @Expose
    @SerializedName("support")
    private String support;

    @Expose
    @SerializedName("flights")
    private List<Integer> flights = new ArrayList<Integer>();

    public Placement(String adView, String support, List<Integer> flights) {
        super();
        this.adView = adView;
        this.support = support;
        this.flights = flights;
    }

    public String getAdView() {
        return adView;
    }

    public void setAdView(String adView) {
        this.adView = adView;
    }

    public String getSupport() {
        return support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

    public List<Integer> getFlights() {
        Collections.sort(flights);
        return flights;
    }

    public void setFlights(List<Integer> flights) {
        this.flights = flights;
    }

    public static void main(String[] args) {
        
    }

}
