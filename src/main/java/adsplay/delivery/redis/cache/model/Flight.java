package adsplay.delivery.redis.cache.model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flight extends RedisModel {

    public static final String KEY_NAME = "flight";

    @SerializedName("flightId")
    private int flightId = 0;

    @Expose
    @SerializedName("categories")
    private List<String> categories = new ArrayList<String>();

    @Expose
    @SerializedName("source_provider")
    private List<Integer> sourceProvider = new ArrayList<Integer>();

    @Expose
    @SerializedName("timezone")
    private String timeZone = "Asia/Ho_Chi_Minh";

    @Expose
    @SerializedName("date_range")
    private List<DateRange> dateRange = new ArrayList<DateRange>();

    @Expose
    @SerializedName("hour")
    private List<Integer> hours = new ArrayList<Integer>();

    @Expose
    @SerializedName("day_week")
    private List<Integer> dayWeek = new ArrayList<Integer>();

    @Expose
    @SerializedName("location")
    private List<String> locations = new ArrayList<String>();

    @Expose
    @SerializedName("frequency")
    private List<Integer> frequencies = new ArrayList<Integer>();

    @Expose
    @SerializedName("weight")
    private int weight = 0;

    @Expose
    @SerializedName("total_booking")
    private int totalBooking = 0;

    private int totalTrackingBooking = 0;

    @Expose
    @SerializedName("daily_booking")
    private int dailyBooking = 0;

    private int dailyTrackingBooking = 0;

    @Expose
    @SerializedName("buy_type")
    private String buyType = "";

    @Expose
    @SerializedName("third_trackings")
    private List<ThirdPTTracking> list3rdTracking = new ArrayList<ThirdPTTracking>();

    @Expose
    @SerializedName("booking_type")
    private String bookingType;
    
    @Expose
    @SerializedName("campaign_id")
    private int campaign_id;

    @Expose
    @SerializedName("creatives")
    List<Integer> creativeIds = new ArrayList<Integer>();

    public Flight(int flightId, List<String> categories, List<Integer> sourceProvider, String timeZone, List<DateRange> dateRange,
            List<Integer> hours, List<Integer> dayWeek, List<String> locations, List<Integer> frequencies, int weight, int totalBooking,
            int totalTrackingBooking, int dailyBooking, int dailyTrackingBooking, String buyType, List<ThirdPTTracking> list3rdTracking,
            String bookingType, List<Integer> creativeIds) {
        super();
        this.flightId = flightId;
        this.categories = categories;
        this.sourceProvider = sourceProvider;
        this.timeZone = timeZone;
        this.dateRange = dateRange;
        this.hours = hours;
        this.dayWeek = dayWeek;
        this.locations = locations;
        this.frequencies = frequencies;
        this.weight = weight;
        this.totalBooking = totalBooking;
        this.totalTrackingBooking = totalTrackingBooking;
        this.dailyBooking = dailyBooking;
        this.dailyTrackingBooking = dailyTrackingBooking;
        this.buyType = buyType;
        this.list3rdTracking = list3rdTracking;
        this.bookingType = bookingType;
        this.creativeIds = creativeIds;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public List<Integer> getSourceProvider() {
        return sourceProvider;
    }

    public void setSourceProvider(List<Integer> sourceProvider) {
        this.sourceProvider = sourceProvider;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public List<DateRange> getDateRange() {
        return dateRange;
    }

    public void setDateRange(List<DateRange> dateRange) {
        this.dateRange = dateRange;
    }

    public List<Integer> getHours() {
        return hours;
    }

    public void setHours(List<Integer> hours) {
        this.hours = hours;
    }

    public List<Integer> getDayWeek() {
        return dayWeek;
    }

    public void setDayWeek(List<Integer> dayWeek) {
        this.dayWeek = dayWeek;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public List<Integer> getFrequencies() {
        return frequencies;
    }

    public void setFrequencies(List<Integer> frequencies) {
        this.frequencies = frequencies;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getBuyType() {
        return buyType;
    }

    public void setBuyType(String buyType) {
        this.buyType = buyType;
    }

    public List<ThirdPTTracking> getList3rdTracking() {
        return list3rdTracking;
    }

    public void setList3rdTracking(List<ThirdPTTracking> list3rdTracking) {
        this.list3rdTracking = list3rdTracking;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public List<Integer> getCreativeIds() {
        return creativeIds;
    }

    public void setCreativeIds(List<Integer> creativeIds) {
        this.creativeIds = creativeIds;
    }

    public int getTotalBooking() {
        return totalBooking;
    }

    public void setTotalBooking(int totalBooking) {
        this.totalBooking = totalBooking;
    }

    public int getDailyBooking() {
        return dailyBooking;
    }

    public void setDailyBooking(int dailyBooking) {
        this.dailyBooking = dailyBooking;
    }

    public int getTotalTrackingBooking() {
        return totalTrackingBooking;
    }

    public void setTotalTrackingBooking(int totalTrackingBooking) {
        this.totalTrackingBooking = totalTrackingBooking;
    }

    public int getDailyTrackingBooking() {
        return dailyTrackingBooking;
    }

    public void setDailyTrackingBooking(int dailyTrackingBooking) {
        this.dailyTrackingBooking = dailyTrackingBooking;
    }
    
    public int getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(int campaign_id) {
        this.campaign_id = campaign_id;
    }

    public static void main(String[] args) throws FileNotFoundException {
        
    }

}
